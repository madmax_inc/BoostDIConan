from conans import ConanFile, CMake, tools


class BoostDIConan(ConanFile):
    name = "boost-di"
    version = "0.1.0"
    license = "<Put the package license here>"
    url = "https://gitlab.com/madmax_inc/BoostDIConan"
    description = "Your C++14 one header only Dependency Injection library with no dependencies"
    no_copy_source = True
    exports_sources = "di/include/*"
    # No settings/options are necessary, this is header only


    def source(self):
        self.run("git clone https://github.com/boost-experimental/di.git")

    def package(self):
        self.copy("di/include/boost/di.hpp", "include", keep_path=False)

    def package_id(self):
            self.info.header_only()
